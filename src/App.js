import React, { Component } from 'react';
import NewTemplateEditor from "./components/Layout/NewTemplateEditor";
import { BrowserRouter, Route } from 'react-router-dom';



class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Route path='/home' component={NewTemplateEditor} />
        </div>
      </BrowserRouter> 
    )
  }
}

export default App
