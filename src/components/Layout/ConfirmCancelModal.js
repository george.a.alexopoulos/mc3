import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import '../css/ConfirmCancelModal.css';

function ConfirmCancelModal(props) {
    return ReactDOM.createPortal (
      <Fragment>
        <div className="overlay-styles" />
          <div className="modal-styles">
            <div className="container">
              <div className="container-child-box-1">
                {props.children}
              </div>
              <div className="container-child-box-2">
                <button 
                  onClick={props.onNo}
                  className="button-no"
                >
                  NO
                </button>
                <button 
                  onClick={props.onYes}
                  className="button-yes"
                >
                  YES
                </button>
              </div>
            </div>
        </div>
      </Fragment>,
      document.getElementById('portal')
    );
}

export default ConfirmCancelModal
