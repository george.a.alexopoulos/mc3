import React, { Component } from 'react';
import '../css/NewCreateNewTemplate.css';
import firebase from "../../firebase";
import { v4 as uuidv4 } from "uuid";

      
class NewCreateNewTemplate extends Component {
  
  constructor(props) {
    super(props)
    
    this.state = {
      id: '',
      date: '',
      from: '',
      subject: '',
      bodyhtml: '', 
      vendorid: '', 
      vendoremail: '', 
      orderrules: '', 
      deliveryrules: '', 
      removalselectors: '', 
      ordernumberphrase: '', 
      imagenminus: '', 
      fragmentexclusion: '', 
      chunkexclusion: '', 
      nfragments: '', 
      trackinglinkpattern: '', 
      trackingnumberphrase: '', 
      createdAt: '',
      lastUpdate: '',
      editNew: false,
      errorVendorid: false,
      errorVendoremail: false,
    };
    // this.handleChange = this.handleChange.bind(this);
    // this.handleSaveClick = this.handleSaveClick.bind(this);
    // this.handleClassNameVendorid = this.handleClassNameVendorid.bind(this);
  }
  
  //ONE HANDLE CHANGE FUNCTION TO RULE VENDOR ID 
  handleChangevendorid = event => {
    this.setState({ 
      [event.target.name] : event.target.value ,
      errorVendorid: false,
    });
  }

  //ONE HANDLE CHANGE FUNCTION TO RULE VENDOR ID   
  handleChangeVendoremail = event => {
    this.setState({ 
      [event.target.name] : event.target.value ,
      errorVendoremail: false,
    });
  };
        
  //ONE HANDLE CHANGE FUNCTION TO RULE THEM ALL (THE REST OF INPUTS)  
  handleChange = event => {
    this.setState({ [event.target.name] : event.target.value });
  }

  // ADD/SAVE FUNCTION
  handleSaveClick = event => {
    event.preventDefault();
    const ref = firebase.firestore().collection("templates");
    const newTemplate = {
      id: uuidv4(),
      date:this.state.date,
      from:this.state.from,
      subject:this.state.subject,
      bodyhtml:this.state.bodyhtml, 
      vendorid:this.state.vendorid, 
      vendoremail:this.state.vendoremail, 
      orderrules:this.state.orderrules, 
      deliveryrules:this.state.deliveryrules, 
      removalselectors:this.state.removalselectors, 
      ordernumberphrase:this.state.ordernumberphrase, 
      imagenminus:this.state.imagenminus, 
      fragmentexclusion:this.state.fragmentexclusion, 
      chunkexclusion:this.state.chunkexclusion, 
      nfragments:this.state.nfragments, 
      trackinglinkpattern:this.state.trackinglinkpattern, 
      trackingnumberphrase:this.state.trackingnumberphrase,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      lastUpdate: firebase.firestore.FieldValue.serverTimestamp(),      
    };
    
    //REGISTER INPUT VALUES TO FIRESTORE
    if(this.isNotValidTemplate(newTemplate)) {
      ref
        .doc(newTemplate.id)
        .set(newTemplate)
        .catch((err) => {
          console.error(err);
        });

      this.setState({
        id: '',
        date: '',
        from: '',
        subject: '',
        bodyhtml: '', 
        vendorid: '', 
        vendoremail: '', 
        orderrules: '', 
        deliveryrules: '', 
        removalselectors: '', 
        ordernumberphrase: '', 
        imagenminus: '', 
        fragmentexclusion: '', 
        chunkexclusion: '', 
        nfragments: '', 
        trackinglinkpattern: '', 
        trackingnumberphrase: '', 
        createdAt: '',
        lastUpdate: '',
        editNew: false,
        errorVendorid: false,
        errorVendoremail: false,
        }, ()=>this.props.saveEditNew(this.state.editNew) );
    } else alert('One or more fields are invalid!!'); 
	}

  isNotValidTemplate(template) {
    if (
      template.vendorid === '' &&
      template.vendoremail === ''
      ) {
        this.setState({ 
          errorVendorid: true ,
          errorVendoremail: true ,
        });
    } else if (
        template.vendorid === '' &&
        template.vendoremail !== ''
    ) {
      this.setState({ errorVendorid: true });
    } else if (
      template.vendorid !== '' &&
      template.vendoremail === ''
    ) {
      this.setState({ errorVendoremail: true });
    } else if (
      // template.id
      typeof template.id === 'string' &&
      template.id !== '' &&
      // template.date
      typeof template.date === 'string' &&
      // template.from
      typeof template.from === 'string' &&
      // template.subject
      typeof template.subject === 'string' &&
      // template.bodyhtml
      typeof template.bodyhtml === 'string' &&
      // template.vendorid
      typeof template.vendorid === 'string' &&
      // template.vendorid !== '' &&
      // template.vendoremail
      typeof template.vendoremail === 'string' &&
      // template.vendoremail !== '' &&
      // template.orderrules
      typeof template.orderrules === 'string' &&
      // template.deliveryrules
      typeof template.deliveryrules === 'string' &&
      // template.removalselectors
      typeof template.removalselectors === 'string' &&
      // template.ordernumberphrase
      typeof template.ordernumberphrase === 'string' &&
      // template.imagenminus
      typeof template.imagenminus === 'string' &&
      // template.fragmentexclusion
      typeof template.fragmentexclusion === 'string' &&
      // template.chunkexclusion
      typeof template.chunkexclusion === 'string' &&
      // template.nfragments
      typeof template.nfragments === 'string' &&
      // template.trackinglinkpattern
      typeof template.trackinglinkpattern === 'string' &&
      // template.trackingnumberphrase
      typeof template.trackingnumberphrase === 'string' &&
      // template.createdAt
      typeof template.createdAt === 'object' &&
      template.createdAt !== '' &&
      // template.lastUpdate
      typeof template.lastUpdate === 'object' &&
      template.lastUpdate !== '' 
    ){
      return true ;
    } else return false;
  };

  // CANCEL EDIT OR EXIT FUNCTION
  cancelEditNewClick = event => {
    this.setState({
      editNew: false
    }, ()=>this.props.cancelEditNewClick(this.state.editNew));	
  } 

  render() {
    return (
      <form className="fill-window">
        <div className="flex-container">
          <div>
            <header className="header">
              <h1>
                New Template Details
              </h1>
            </header>
          </div>
        </div>
        <div className="flex-container1">
          <div className="flex-child1">
            <h2>
            New Vendor Email Details For Testing
            </h2>
            <div className="flex-container3">
              <label className="flex-container3label">
              Date :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="date"
                  value={this.state.date}      
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
              From :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="from"   
                  value={this.state.from}      
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
              Subject :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="subject"   
                  value={this.state.subject}      
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
          </div>
          <div className="flex-child1">
            <h2>
              Paste The Email html body To Test here
            </h2>
            <div className="flex-container1">
              <textarea  
                className="flex-containerInput4"
                placeholder="HTML BODY"
                name="bodyhtml"   
                value={this.state.bodyhtml}      
                onChange={this.handleChange.bind(this)}
                />
            </div>
          </div> 
          <div className="flex-child2"></div>      
        </div>
        <div className="flex-containervendorrules">
          <div className="flex-containervendorruleschild1">
            <h2>
            Vendor Rules
            </h2>
            <div className="flex-container3">
              <label 
                className="flex-container3label"
                style={{color: this.state.errorVendorid ? 'red' : ''}}
              >
                vendor id :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="vendorid"   
                  value={this.state.vendorid}      
                  onChange={this.handleChangevendorid.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label
                className="flex-container3label"
                style={{color: this.state.errorVendoremail ? 'red' : ''}}
              >
                vendor email : 
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="vendoremail"
                  value={this.state.vendoremail}         
                  onChange={this.handleChangeVendoremail.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                order rules :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="orderrules"   
                  value={this.state.orderrules}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                delivery rules :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="deliveryrules"   
                  value={this.state.deliveryrules}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                removal selectors :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="removalselectors"   
                  value={this.state.removalselectors}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                ordernumber phrase :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="ordernumberphrase"   
                  value={this.state.ordernumberphrase}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                image n minus :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="imagenminus"   
                  value={this.state.imagenminus}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                fragment exclusion :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="fragmentexclusion"   
                  value={this.state.fragmentexclusion}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                chunk exclusion :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="chunkexclusion"   
                  value={this.state.chunkexclusion}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                n fragments :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="nfragments"   
                  value={this.state.nfragments}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                tracking link pattern :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="trackinglinkpattern"   
                  value={this.state.trackinglinkpattern}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                tracking number phrase :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="trackingnumberphrase"   
                  value={this.state.trackingnumberphrase}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
          </div>
          <div className="flex-containervendorruleschild2">
            <h2>
              TEST RESULTS
            </h2>
          </div>  
          <div className="flex-containervendorruleschild3">
            <button className="buttontest">TEST TEMPLATE</button>
            <button
              className="buttonsave"
              onClick={this.handleSaveClick}
              >SAVE
            </button>
            <button
              className="buttonCancel"
              onClick={this.cancelEditNewClick}
              >
              CANCEL OR EXIT
            </button>
          </div>
        </div>      
      </form>
    )
  }
}

export default NewCreateNewTemplate

