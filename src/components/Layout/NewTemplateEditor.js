import React, { Component } from 'react';
import TopNav from './TopNav';
import NewCreateNewTemplate from './NewCreateNewTemplate';
import firebase from "../../firebase";
import EditSelectedTemplate from './EditSelectedTemplate';
import '../css/NewTemplateEditor.css'


class NewTemplateEditor extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      editSelected: false,
      editNew: false, 
      loading: false,
      templates: [],
      selectedTemplate: [] ,
    }
  }



  // FETCH TEMPLATES
  componentDidMount() {
    const ref = firebase.firestore().collection("templates").orderBy('createdAt', 'desc');
    // set Loading to true
    this.setState({
      loading: true,
    });
    ref.onSnapshot((querySnapshot) => {
      const items = [];
      querySnapshot.forEach((doc) => {
        items.push(doc.data());
      });
      // setTemplates(items);
      this.setState({
        templates: items,
      });
      // setLoading(false);
      this.setState({
        loading: false,
      });
      // console.log(this.state.templates.map(template => template.vendorid ))
    });
  }

  // componentDidUpdate(prevProps, prevState) {
  //   console.log("i updated NTE")
  //   console.log("prevProps :", prevProps)
  //   console.log("prevState", prevState)
  // }






  // CREATE NEW FUNCTION
  handleEditNewClick = event => {
    this.setState({
      editNew: true,
    });
  };

  // SAVE EDIT NEW
  handleSaveEditNew = (editNewState) => {
    this.setState({
      editNew: editNewState,
    });
  };

  // CANCEL CREATE NEW
  handlecancelEditNewClick = (editNewState) => {
    this.setState({
      editNew: editNewState,
    });
  };

  // EDIT SELECTED TEMPLATE
  editSelectedTemplate  = (template) => {
    this.setState({
      selectedTemplate: template,
      editSelected: true,
    });
  } 

  // HANDLE UPDATE EDIT SELECTED
  handleEditSelected = (editSelectedState) => {
    this.setState({
      editSelected: editSelectedState,
    })
  }
  // HANDLE CANCEL EDIT SELECTED
  handleCancelEditSelected = (editSelectedState) => {
    this.setState({
      editSelected: editSelectedState,
    });
  }


  render() {
    
    return  this.state.editNew && !this.state.editSelected 
    ? (
      <NewCreateNewTemplate 
        editNew={this.state.editNew} 
        cancelEditNewClick={this.handlecancelEditNewClick}
        saveEditNew={this.handleSaveEditNew}
      />
    ) : 
    this.state.editSelected && !this.state.editNew
    ? (
      <EditSelectedTemplate 
        editSelected={this.state.editSelected} 
        selectedTemplate={this.state.selectedTemplate}
        cancelEditSelected={this.handleCancelEditSelected}
        updateEditSelected={this.handleEditSelected}
      />
    ) : (
      <div className="fill-window">
        <div className="flex-container">
          <div className="flex-container-child-1">
            <TopNav />
            <h4>space for the topnav</h4>
          </div>
          <div className="flex-container-child-2">
            <h1>Template Editor</h1>
          </div>
          <div className="flex-container-child-3">
            <div className="button-div">
              <button className="button"
                onClick={this.handleEditNewClick}
              >Create New Template
              </button>
            </div>
          </div>
          <div className="flex-container-child-4">
            <h2>Saved Templates</h2>
              {this.state.loading ? <h1>Loading...</h1> : null}
              {this.state.templates.map(template => (
                <button
                  key={template.id}
                  className="flex-container-BoxForEachTemplate"
                  onClick={ () => this.editSelectedTemplate(template)}
                >
                  Vendor ID :  {template.vendorid}   Vendor Email:  {template.vendoremail} 
                </button>
              ))}
          </div>
        </div>
      </div>
    );
  }
}

export default NewTemplateEditor

