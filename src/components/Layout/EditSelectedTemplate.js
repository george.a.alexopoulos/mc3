import React, { Component } from 'react';
import '../css/EditSelectedTemplate.css';
import firebase from "../../firebase";
import ConfirmCancelModal from './ConfirmCancelModal';

      
class EditSelectedTemplate extends Component {
  
  constructor(props) {
    super(props)
    
    this.state = {
      id: this.props.selectedTemplate.id,
      date: this.props.selectedTemplate.date,
      from: this.props.selectedTemplate.from,
      subject: this.props.selectedTemplate.subject,
      bodyhtml: this.props.selectedTemplate.bodyhtml, 
      vendorid: this.props.selectedTemplate.vendorid, 
      vendoremail: this.props.selectedTemplate.vendoremail, 
      orderrules: this.props.selectedTemplate.orderrules, 
      deliveryrules: this.props.selectedTemplate.deliveryrules, 
      removalselectors: this.props.selectedTemplate.removalselectors, 
      ordernumberphrase: this.props.selectedTemplate.ordernumberphrase, 
      imagenminus: this.props.selectedTemplate.imagenminus, 
      fragmentexclusion: this.props.selectedTemplate.fragmentexclusion, 
      chunkexclusion: this.props.selectedTemplate.chunkexclusion, 
      nfragments: this.props.selectedTemplate.nfragments, 
      trackinglinkpattern: this.props.selectedTemplate.trackinglinkpattern, 
      trackingnumberphrase: this.props.selectedTemplate.trackingnumberphrase, 
      createdAt: this.props.selectedTemplate.createdAt,
      lastUpdate: this.props.selectedTemplate.lastUpdate,
      editSelected: this.props.editSelected,
      isOpenModal: false,      
    };
  }
  
        
        
        
  //ONE HANDLE CHANGE FUNCTION TO RULE THEM ALL (INPUTS)  
  handleChange = event => {
    this.setState({ 
      [event.target.name] : event.target.value 
    });
  }
  
 
  
  // UPDATE FUNCTION 2
  handleUpdateClick = (event) => {
    event.preventDefault();
    const updateRef = firebase.firestore().collection("templates").doc(this.state.id);
    const updatedTemplate = {
      date:this.state.date,
      from:this.state.from,
      subject:this.state.subject,
      bodyhtml:this.state.bodyhtml, 
      orderrules:this.state.orderrules, 
      deliveryrules:this.state.deliveryrules, 
      removalselectors:this.state.removalselectors, 
      ordernumberphrase:this.state.ordernumberphrase, 
      imagenminus:this.state.imagenminus, 
      fragmentexclusion:this.state.fragmentexclusion, 
      chunkexclusion:this.state.chunkexclusion, 
      nfragments:this.state.nfragments, 
      trackinglinkpattern:this.state.trackinglinkpattern, 
      trackingnumberphrase:this.state.trackingnumberphrase, 
      lastUpdate: firebase.firestore.FieldValue.serverTimestamp(),      
    };
    
    updateRef
    .update(updatedTemplate)
    .then(() => {
      console.log("Document updated"); // Document updated
      console.log(updatedTemplate)
    })
    .catch((error) => {
      console.error("Error updating doc", error);
      alert('something went wrond');
    });	

    this.setState({
      editSelected: false,
    }, ()=>this.props.updateEditSelected(this.state.editSelected));
  };
  
  
  // CANCEL EDIT SELECTED TEMPLATE OR EXIT FUNCTION
  justcancelEditSelected = () => {
    this.setState({
      isOpenModal: false,
      editSelected: false
    }, () => this.props.cancelEditSelected(this.state.editSelected, this.state.isOpenModal));	
  }  

  //SHOW MODAL FUNCTION
  showModal = (event) => {
    event.preventDefault();

    console.log(this.props.selectedTemplate.orderrules );
    console.log(this.state.orderrules );

    if (
      this.props.selectedTemplate.date  === this.state.date &&
      this.props.selectedTemplate.from  === this.state.from &&
      this.props.selectedTemplate.subject  === this.state.subject &&
      this.props.selectedTemplate.bodyhtml  === this.state.bodyhtml &&
      this.props.selectedTemplate.orderrules  === this.state.orderrules &&
      this.props.selectedTemplate.deliveryrules  === this.state.deliveryrules &&
      this.props.selectedTemplate.removalselectors  === this.state.removalselectors &&
      this.props.selectedTemplate.ordernumberphrase  === this.state.ordernumberphrase &&
      this.props.selectedTemplate.imagenminus  === this.state.imagenminus &&
      this.props.selectedTemplate.fragmentexclusion  === this.state.fragmentexclusion &&
      this.props.selectedTemplate.chunkexclusion  === this.state.chunkexclusion &&
      this.props.selectedTemplate.nfragments  === this.state.nfragments &&
      this.props.selectedTemplate.trackinglinkpattern  === this.state.trackinglinkpattern &&
      this.props.selectedTemplate.trackingnumberphrase  === this.state.trackingnumberphrase 
      ) {
      // console.log('I just need to cancel');
      this.justcancelEditSelected()
    } else {
      // console.log('I need to confirm')
      this.setState({ isOpenModal: true });
    };
  };

  // CALLBACK FOR MODAL ISOPEN
  handleIsOpenModal = () => {
    this.setState({
      isOpenModal: false
    })
  }
  
  render() {
    return (
      <form className="fill-window">
        <div>
          <header className="header">
            <h1>
              Template Details
            </h1>
          </header>
        </div>
        <div className="flex-container1">
          <div className="flex-child1">
            <h2>
            New Vendor Email Details For Testing
            </h2>
            <div className="flex-container3">
              <label className="flex-container3label">
              Date :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="date"
                  value={this.state.date}      
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
              From :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="from"   
                  value={this.state.from}      
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
              Subject :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="subject"   
                  value={this.state.subject}      
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
          </div>
          <div className="flex-child1">
            <h2>
              Paste The Email html body To Test here
            </h2>
            <div className="flex-container1">
              <textarea  
                className="flex-containerInput4"
                placeholder="HTML BODY"
                name="bodyhtml"   
                value={this.state.bodyhtml}      
                onChange={this.handleChange.bind(this)}
                />
            </div>
          </div> 
          <div className="flex-child2"></div>      
        </div>
        <div className="flex-containervendorrules">
          <div className="flex-containervendorruleschild1">
            <h2>
            Vendor Rules
            </h2>
            <div className="flex-container3">
              <label className="flex-container3label">
                vendor id :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="vendorid"   
                  readOnly     
                  value={this.state.vendorid}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                vendor email : 
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="vendoremail"
                  readOnly  
                  value={this.state.vendoremail}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                order rules :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="orderrules"   
                  value={this.state.orderrules}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                delivery rules :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="deliveryrules"   
                  value={this.state.deliveryrules}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                removal selectors :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="removalselectors"   
                  value={this.state.removalselectors}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                ordernumber phrase :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="ordernumberphrase"   
                  value={this.state.ordernumberphrase}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                image n minus :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="imagenminus"   
                  value={this.state.imagenminus}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                fragment exclusion :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="fragmentexclusion"   
                  value={this.state.fragmentexclusion}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                chunk exclusion :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="chunkexclusion"   
                  value={this.state.chunkexclusion}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                n fragments :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="nfragments"   
                  value={this.state.nfragments}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                tracking link pattern :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="trackinglinkpattern"   
                  value={this.state.trackinglinkpattern}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
            <div className="flex-container3">
              <label className="flex-container3label">
                tracking number phrase :
              </label>
              <input
                  className="flex-container3Input"
                  type="text"
                  name="trackingnumberphrase"   
                  value={this.state.trackingnumberphrase}         
                  onChange={this.handleChange.bind(this)}
                  />
            </div>
          </div>
          <div className="flex-containervendorruleschild2">
            <h2>
              TEST RESULTS
            </h2>
          </div>  
          <div className="flex-containervendorruleschild3">
            <button className="buttontest">TEST TEMPLATE</button>
            <button
              className="buttonsave"
              onClick={this.handleUpdateClick}
              >UPDATE TEMPLATE
            </button>
            <button
              className="buttonCancel"
              onClick={this.showModal}
              >
              CANCEL
            </button>
           
          </div>
          {
            this.state.isOpenModal &&
            <ConfirmCancelModal 
              onYes={this.justcancelEditSelected}
              onNo={this.handleIsOpenModal}
            >
              <h1>Are you sure you want to discard your changes ?</h1>
            </ConfirmCancelModal>
          }
        </div>      
      </form>
    );
  }
}

export default EditSelectedTemplate

